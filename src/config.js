import audio from "./assets/music_tension.mp3"
const config = {
    "url_api": "http://3.15.100.78:8080/",
    "options": function (data) {
        return {
            "method": "POST",
            "body": data
        }
    }
};

const localdb = {
    getId: function () {
        let credentials = JSON.parse(localStorage.getItem('session-user'));
        return credentials.id;
    }
};
const sound = {
    el: document.createElement("audio"),
    init: function () {
        this.el.src = audio;
        this.el.volume = 0.2;
        this.el.addEventListener("ended", e => this.el.play());
    },

    play: function () {
        this.el.play()
    },
    pause: function () {
        this.el.pause()
    }
};
const format = {
    clear: function (number) {
        if (number.length > 3) {
            if (number.indexOf(",") >= 0) {
                number = number.replace(/,/g, "")
            }
            if (number.indexOf(".") >= 0) {
                number = number.replace(/\./g, "")
            }
        }
        return number
    },
    convert: function (number) {
        if (typeof number == "number") {
            number = number.toString()
        }
        if (number.length > 3) {
            return parseInt(number).toLocaleString("es-ES");
        }
        return number
    }
}
export { localdb, sound, format }
export default config;

