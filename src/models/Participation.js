import config from "../config"
export default class Participation {
    constructor() {
        this.endpoint = "participations"
    }
    async create(data) {
        try {
            const req = await fetch(`${config.url_api}${this.endpoint}`, config.options(data))
            const res = await req.json();
            if (req.ok) {
                return res;
            } else {
                return false;
            }
        } catch (err) {
            console.error(err)
            return false;
        }
    }


    async all() {
        try {
            const req = await fetch(`${config.url_api}${this.endpoint}`)
            const res = await req.json();
            if (req.ok) {
                return res.data;
            } else {
                return [];
            }
        } catch (err) {
            console.error(err)
            return [];
        }
    }


}