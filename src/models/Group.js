import config from "../config"
export default class Group {
    constructor() {
        this.endpoint = "groups"
    }
    async create(data) {
        try {
            const req = await fetch(`${config.url_api}${this.endpoint}`, config.options(data))
            const res = await req.json();

            if (req.ok) {
                return res.data;
            } else {
                return false;
            }
        } catch (err) {
            console.error(err)
            return false;
        }
    }

    async all() {
        try {
            const req = await fetch(`${config.url_api}${this.endpoint}`)
            const res = await req.json();
            if (req.ok) {
                return res.data;
            } else {
                return [];
            }
        } catch (err) {
            console.error(err)
            return [];
        }
    }

}