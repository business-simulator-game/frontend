import config from "../config"
export default class User {

    async login(credentials) {

        try {
            const req = await fetch(`${config.url_api}login`, config.options(credentials))
            const user = await req.json();
            if (req.ok) {
                return user.user;
            } else {
                return false;
            }
        } catch (err) {
            console.error(err)
            return false;
        }
    }

    async create(data) {
        try {

            const req = await fetch(`${config.url_api}users`, config.options(data))
            return req.ok
        } catch (err) {
            console.error(err)
            return [];
        }
    }

    async logout() {
        try {
            const req = await fetch(`${config.url_api}logout`)
            return req.ok
        } catch (err) {
            console.error(err)
            return false;
        }
    }
    async all() {
        try {
            const req = await fetch(`${config.url_api}users`)
            const users = await req.json();
            if (req.ok) {
                return users;
            } else {
                return [];
            }
        } catch (err) {
            console.error(err)
            return [];
        }
    }

}