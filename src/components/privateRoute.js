import React from 'react';
import { Route, Redirect } from 'react-router-dom';

const PrivateRoute = ({ component: Component, auth, ...rest }) => {
    const session = localStorage.getItem('session-user');
    return (
        <Route
            {...rest}
            // render={props => (auth.isAuthenticated === true ? (<Component {...props} />) : (<Redirect to="/login" />))}
            render={(props) =>
                session ? <Component {...props} /> : <Redirect to="/login" />
            }
        />
    );
};

export default PrivateRoute;
