
import React, { Component, Fragment } from 'react';
import {
    Card,
    CardContent,
    Container,
    Button,
    Grid,
    TextField,
} from '@material-ui/core';
import IconAdd from "@material-ui/icons/Add"
import Loader from "./../default/loader"
import { AlertError, AlertSuccess } from "./../default/alert"
import Group from "./../../models/Group"
import Navbar from './../default/navbar'
export default class GroupCreate extends Component {
    constructor(props) {
        super(props);
        this.model = new Group();
        this.state = {
            loader: false,
            error: false,
            success: false
        }
        this.handleSubmit = this.handleSubmit.bind(this)
    }
    async handleSubmit(e) {
        e.preventDefault()
        this.setState({ loader: true });
        let data = new FormData(e.target);
        let res = await this.model.create(data);
        let newState = this.state;
        if (res) {
            newState.error = false;
            newState.success = true;
            e.target.reset();
        } else {
            newState.error = true;
            newState.success = false;
        }
        newState.loader = false;
        this.setState({ ...newState });
    }
    render() {
        return (
            <Fragment>
                <Navbar />
                <Container >
                    <h2 style={{ textAlign: "center" }}>Registra un grupo</h2>
                    <Grid container spacing={2}>
                        <Grid item xs={12} sm={12} md={3} />
                        <Grid item xs={12} sm={6} md={6}>
                            <Card elevation={2}>
                                {this.state.error ? (<AlertError message="Error, Húbo un problema!" />) : ('')}
                                {this.state.success ? (<AlertSuccess message="Hemos registrado el grupo!" />) : ('')}
                                {this.state.loader ? (<Loader />) : ("")}
                                <CardContent style={{ textAlign: "center" }}>
                                    <form onSubmit={this.handleSubmit}>
                                        <TextField
                                            variant="outlined"
                                            name="name"
                                            label="Ingresa el nombre del grupo"
                                            style={{ width: 290 }}
                                            required
                                        />
                                        <Button type="submit" style={{ marginTop: "10px ", marginLeft: "10px" }} variant="contained" color="secondary"  >
                                            <IconAdd /> &nbsp; Crear
                                    </Button>
                                    </form>
                                </CardContent>

                            </Card>
                        </Grid>
                        <Grid item xs={12} sm={12} md={3} />
                    </Grid>
                </Container>
            </Fragment>
        )
    }
}