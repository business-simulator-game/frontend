
import React, { Component, Fragment } from 'react';
import {
    Card,
    CardContent,
    Container,
    Grid,
} from '@material-ui/core';
import Group from '../../models/Group';
import Loader from "./../default/loader"

import Navbar from './../default/navbar'
import EmptyMessage from '../default/emptyMessage';
export default class GroupCreate extends Component {
    constructor(props) {
        super(props);
        this.model = new Group();
        this.state = {
            loader: false,
            data: []
        }

    }
    componentDidMount() {
        this.loadGroups();
    }
    async loadGroups() {
        const data = await this.model.all()
        this.setState({
            loader: false,
            data: data
        })
    }
    render() {
        return (
            <Fragment>
                <Navbar />
                <Container >
                    <h2>Listado de grupos</h2>
                    {this.state.loader ? (<Loader />) : ("")}
                    <Grid container spacing={2}>
                        {
                            this.state.data.map(({ pk, fields: data }) => (
                                <Grid key={pk} item xs={12} sm={6} md={4}>
                                    <Card elevation={2}>
                                        <CardContent>
                                            {data.name}<br />
                                        </CardContent>
                                    </Card>
                                </Grid>
                            ))
                        }
                    </Grid>
                </Container>
                {this.state.data.length ? ("") : (<EmptyMessage title="Woops!" message="No hay grupos registrados" />)}
            </Fragment>
        )
    }
}