import React, { Fragment } from "react"
import {
    Card,
    CardContent,
    Container,
    Grid,
} from '@material-ui/core';
/*
 * props.title
 * props.message
 */
const EmptyMessage = (props) => {
    return (
        <Fragment>
            <Container >
                <Grid container spacing={2}>
                    <Grid item xs={12} sm={12} md={3} />
                    <Grid item xs={12} sm={6} md={6}>
                        <Card elevation={3}>
                            <CardContent style={{ textAlign: "center" }}>
                                <h4>{props.title}</h4>
                                <p><em>{props.message}</em></p>
                            </CardContent>

                        </Card>
                    </Grid>
                    <Grid item xs={12} sm={12} md={3} />
                </Grid>
            </Container>
        </Fragment>
    )
}
export default EmptyMessage;