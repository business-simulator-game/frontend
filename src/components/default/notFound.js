import React from 'react';
import { useHistory } from 'react-router-dom';
import { Container, Button } from '@material-ui/core';

const NotFound = () => {
    const history = useHistory();
    return (
        <Container>
            <h1>Error 404 </h1>
            <hr />
            <h3>No hemos encontrado la ruta solicitada</h3>
            <Button onClick={() => history.push('/home')} color="primary">
                <b>Volver al Home</b>
            </Button>
        </Container>
    );
};

export default NotFound;
