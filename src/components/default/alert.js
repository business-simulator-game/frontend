
import Alert from "@material-ui/lab/Alert"
const AlertSuccess = ({ message }) => {
    return (
        <Alert severity="success">{message}</Alert>
    )
}
const AlertError = ({ message }) => {
    return (
        <Alert severity="error">{message}</Alert>
    )
}

const AlertWarning = ({ message }) => {
    return (
        <Alert severity="warning">{message}</Alert>
    )
}


export { AlertSuccess, AlertError, AlertWarning }