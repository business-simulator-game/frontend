import React from "react"
import { CircularProgress } from '@material-ui/core';
const Loader = () => {
    return (
        <div style={{ display: "flex" }}>
            <CircularProgress style={{ margin: "10px auto" }} />
        </div>
    )
}
export default Loader;