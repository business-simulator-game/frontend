import React, { useState, Fragment } from 'react';
import { fade, makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import { Typography, IconButton, Menu, MenuItem } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import { useHistory } from 'react-router-dom';
import AccountCircle from '@material-ui/icons/AccountCircle';
import User from './../../models/User';

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    menuButton: {
        menuButton: theme.spacing(2),
    },
    title: {
        flexFGrow: 1,
    },
    search: {
        position: 'relative',
        borderRadius: theme.shape.borderRadius,
        backgroundColor: fade(theme.palette.common.white, 0.15),
        '&:hover': {
            backgroundColor: fade(theme.palette.common.white, 0.25),
        },
        marginLeft: 0,
        width: '100%',
        [theme.breakpoints.up('sm')]: {
            marginLeft: theme.spacing(1),
            width: 'auto',
        },
    },
    searchIcon: {
        padding: theme.spacing(0, 2),
        height: '100%',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    inputRoot: {
        color: 'inherit',
    },
    inputInput: {
        padding: theme.spacing(1, 1, 1, 0),
        paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('sm')]: {
            width: '12ch',
            '&:focus': {
                width: '20ch',
            },
        },
    },
}));

const Navbar = () => {
    const history = useHistory();
    const classes = useStyles();
    const [anchorEl, setAnchorEl] = useState(null);
    const [anchorEl2, setAnchorEl2] = useState(null);
    const open = Boolean(anchorEl);
    const open2 = Boolean(anchorEl2);
    const [credentials, setCredentials] = useState({
        username: 'Anónimo',
        email: '',
        rendered: true,
    });

    const handleMenu = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };
    const handleCloseSession = async () => {
        setAnchorEl(null);
        const user = new User();
        let res = await user.logout();
        if (res) {
            localStorage.removeItem('session-user');
            history.push('/login');
        } else {
            alert('Error al cerrar sesion');
        }
    };
    const handleClickRedirect = (e) => {
        history.push(e.currentTarget.dataset.route);
    };

    const handleClickMenuGroup = (e) => {
        setAnchorEl2(e.target);
    };
    const handleClose2 = () => {
        setAnchorEl2(null);
    };

    let data = localStorage.getItem('session-user');
    if (data && credentials.rendered) {
        let dataJson = JSON.parse(data);
        setCredentials({
            username: dataJson.username,
            email: dataJson.email,
            is_admin: dataJson.is_admin,
            rendered: false,
        });

    }
    return (
        <div className={classes.root}>
            <AppBar position="static">
                <Toolbar>
                    <Typography variant="h6" className={classes.title}>
                        BusinessSimulator
          </Typography>
                    <div
                        style={{
                            marginLeft: 'auto',
                            marginRight: 0,
                        }}
                    >
                        {credentials.is_admin ? (
                            <Fragment>
                                <Button
                                    onClick={handleClickRedirect}
                                    data-route="/games/create"
                                    color="inherit"
                                    style={{ marginRight: 5 }}
                                >
                                    Crear Juego
                    </Button>
                    <Button
                                    onClick={handleClickRedirect}
                                    data-route="/games/list"
                                    color="inherit"
                                    style={{ marginRight: 5 }}
                                >
                                    Ver Juegos
                    </Button>
                                <Button
                                    color="inherit"
                                    onClick={handleClickMenuGroup}
                                    style={{ marginRight: 5 }}
                                >
                                    Grupos
                                </Button>
                                <Menu
                                    id="menu-appbar"
                                    anchorEl={anchorEl2}
                                    anchorOrigin={{
                                        vertical: 'top',
                                        horizontal: 'right',
                                    }}
                                    keepMounted
                                    transformOrigin={{
                                        vertical: 'top',
                                        horizontal: 'right',
                                    }}
                                    open={open2}
                                    onClose={handleClose2}
                                >

                                    <MenuItem onClick={() => history.push("/groups/list")}>Listado</MenuItem>
                                    <MenuItem onClick={() => history.push("/groups/create")}>Crear</MenuItem>
                                </Menu>
                                <Button
                                    onClick={handleClickRedirect}
                                    data-route="/users"
                                    color="inherit"
                                    style={{ marginRight: 5 }}
                                >
                                    Ver Usuarios
                    </Button>

                            </Fragment>
                        ) : (

                                <Fragment>
                                    <Button
                                        onClick={handleClickRedirect}
                                        data-route="/games/join"
                                        color="inherit"
                                        style={{ marginRight: 5 }}
                                    >
                                        Unirme a un juego
                                 </Button>
                                </Fragment>
                            )}
                    </div>
                    <div
                        style={{
                            marginLeft: 'auto',
                            marginRight: 0,
                        }}
                    >
                        <div
                            aria-label="account of current user"
                            aria-controls="menu-appbar"
                            aria-haspopup="true"
                            onClick={handleMenu}
                        >
                            <IconButton color="inherit">
                                <AccountCircle />
                            </IconButton>
                            {credentials.username}
                        </div>
                        <Menu
                            id="menu-appbar"
                            anchorEl={anchorEl}
                            anchorOrigin={{
                                vertical: 'top',
                                horizontal: 'right',
                            }}
                            keepMounted
                            transformOrigin={{
                                vertical: 'top',
                                horizontal: 'right',
                            }}
                            open={open}
                            onClose={handleClose}
                        >

                            <MenuItem onClick={handleClose}>{credentials.is_admin ? ("Profesor") : ("Estudiante")}</MenuItem>
                            <MenuItem onClick={handleClose}>{credentials.email}</MenuItem>
                            <MenuItem onClick={handleCloseSession}>Cerrar sesión</MenuItem>
                        </Menu>
                    </div>
                </Toolbar>
            </AppBar>
        </div>
    );
};

export default Navbar;
