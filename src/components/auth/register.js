import React, { Component, Fragment } from 'react';
import {
    Card,
    CardActions,
    CardContent,
    Container,
    TextField,
    Button,
    CircularProgress,
    FormControl,
    Select,
    MenuItem,
    InputLabel
} from '@material-ui/core';
import Alert from '@material-ui/lab/Alert';
import User from './../../models/User';
import Group from './../../models/Group';

class Register extends Component {
    constructor(props) {
        super(props);
        this.group = new Group();
        this.user = new User();
        this.state = {
            error: false,
            success: false,
            loader: false,
            groups: [],
        };
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    componentDidMount() {
        this.loadGroups();
    }
    async loadGroups() {
        const res = await this.group.all();
        this.setState({ groups: res });
    }
    async handleSubmit(e) {
        this.setState({
            error: false,
            success: false,
            loader: true,
        });
        e.preventDefault();
        let data = new FormData(e.target);
        let res = await this.user.create(data);
        let newState = this.state;
        if (res) {
            res = await this.user.login(data)
            if (res) {
                localStorage.setItem("session-user", JSON.stringify(res))
                this.props.history.push("/home")
            }
            newState.error = false;
            newState.success = true;
            e.target.reset();
        } else {
            newState.error = true;
            newState.success = false;
        }
        newState.loader = false;
        this.setState({ ...newState });
    }

    render() {
        return (
            <Fragment>
                <Container>
                    <Card
                        elevation={3}
                        style={{ width: 450, margin: '80px auto', marginTop: 30 }}
                    >
                        <CardContent>
                            <h2 style={{ textAlign: 'center' }}>Registrarme</h2>
                            {this.state.error ? (
                                <Alert severity="error">Error algo salió mal</Alert>
                            ) : (
                                    ''
                                )}
                            {this.state.success ? (
                                <Alert severity="success">
                                    Te has registrado correctamente
                                </Alert>
                            ) : (
                                    ''
                                )}
                            {this.state.loader ? (
                                <div style={{ display: 'flex' }}>
                                    <CircularProgress style={{ margin: '0 auto' }} />
                                </div>
                            ) : (
                                    ''
                                )}
                            <form onSubmit={this.handleSubmit}>
                                <FormControl fullWidth>
                                    <InputLabel style={{ margin: 10 }} variant="filled" htmlFor="filled-age-native-simple">
                                        --Selecciona un grupo--
                                    </InputLabel>
                                    <Select
                                        style={{ margin: 10 }}
                                        variant="outlined"
                                        displayEmpty
                                        label="JLJ"
                                        labelId="demo-simple-select-filled-label"
                                        id="demo-simple-select-filled"
                                        name="group_id"
                                    >
                                        {this.state.groups.map(({ pk, fields }, i) => (
                                            <MenuItem value={pk}>{fields.name}</MenuItem>)
                                        )}
                                    </Select>
                                </FormControl>
                                <FormControl fullWidth>
                                    <TextField
                                        style={{ margin: 10 }}
                                        name="email"
                                        variant="outlined"
                                        label="Email"
                                        type="email"
                                        required
                                    />
                                </FormControl>
                                <FormControl fullWidth>
                                    <TextField
                                        style={{ margin: 10 }}
                                        name="username"
                                        variant="outlined"
                                        label="Username"
                                        type="username"
                                        required
                                    />
                                </FormControl>
                                <FormControl fullWidth>
                                    <TextField
                                        style={{ margin: 10 }}
                                        name="password"
                                        variant="outlined"
                                        label="Password"
                                        type="password"
                                        required
                                    />
                                </FormControl>
                                <FormControl fullWidth>
                                    <Button
                                        style={{ marginTop: 30 }}
                                        variant="contained"
                                        color="primary"
                                        type="submit"
                                    >
                                        Registrarme!
                  </Button>
                                </FormControl>
                            </form>
                        </CardContent>
                        <CardActions>
                            <Button
                                size="small"
                                onClick={() => this.props.history.push('/login')}
                                color="primary"
                            >
                                Iniciar sesión
              </Button>
                        </CardActions>
                    </Card>
                </Container>
            </Fragment>
        );
    }
}

export default Register;
