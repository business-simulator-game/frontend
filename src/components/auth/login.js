import React, { Component, Fragment } from 'react';
import User from './../../models/User';
import {
    Card,
    CardActions,
    CardContent,
    Container,
    TextField,
    Button,
    FormControl,
} from '@material-ui/core';
import { AlertSuccess, AlertError } from "./../default/alert"
import Loader from "./../default/loader"
class Login extends Component {
    constructor(props) {
        super(props);
        this.user = new User();
        this.state = {
            error: false,
            success: false,
            loader: false,

        };
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    async handleSubmit(e) {
        e.preventDefault();
        this.setState({ loader: true });
        let credentials = new FormData(e.target);
        let res = await this.user.login(credentials);
        let newState = this.state;
        if (res) {
            newState.error = false;
            newState.success = true;
            localStorage.setItem('session-user', JSON.stringify(res));
            this.props.history.push('/home');

            e.target.reset();
        } else {
            newState.error = true;
            newState.success = false;
        }
        newState.loader = false;
        this.setState({ ...newState });
    }
    render() {
        return (
            <Fragment>
                <Container>
                    <Card
                        elevation={3}
                        style={{ width: 450, margin: '80px auto', marginTop: 30 }}
                    >
                        <CardContent>
                            <h2 style={{ textAlign: 'center' }}>Iniciar Sesión</h2>
                            {this.state.error ? (<AlertError message="Error de autenticación" />) : ('')}
                            {this.state.success ? (<AlertSuccess message="Redireccionando en breve!" />) : ('')}
                            {this.state.loader ? (<Loader />) : ("")}
                            <form onSubmit={this.handleSubmit}>

                                <FormControl fullWidth>
                                    <TextField
                                        style={{ margin: 10 }}
                                        name="username"
                                        variant="outlined"
                                        label="Username"
                                        type="username"
                                        required
                                    />
                                </FormControl>
                                <FormControl fullWidth>
                                    <TextField
                                        style={{ margin: 10 }}
                                        name="password"
                                        variant="outlined"
                                        label="Password"
                                        type="password"
                                        required
                                    />
                                </FormControl>
                                <FormControl fullWidth>
                                    <Button
                                        style={{ marginTop: 30 }}
                                        variant="contained"
                                        color="primary"
                                        type="submit"
                                    >
                                        Iniciar Sesion
                  </Button>
                                </FormControl>
                            </form>
                        </CardContent>
                        <CardActions>
                            <Button
                                onClick={() => this.props.history.push('/register')}
                                size="small"
                                color="primary"
                            >
                                Registrarme
              </Button>
                        </CardActions>
                    </Card>
                </Container>
            </Fragment>
        );
    }
}

export default Login;
