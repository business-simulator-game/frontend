import React, { Component, Fragment } from 'react';
import {
    Card,
    CardActions,
    CardContent,
    Container,
    Button,
    Grid,
} from '@material-ui/core';
import IconDelete from "@material-ui/icons/Delete"
import Game from '../../models/Game';
import Navbar from './../default/navbar'
import EmptyMessage from '../default/emptyMessage';
import Loader from '../default/loader';
export default class GameList extends Component {
    constructor(props) {
        super(props);
        this.model = new Game();
        this.state = {
            loader: true,
            data: []
        }
        this.loadGames = this.loadGames.bind(this)
    }
    componentDidMount() {
        this.loadGames()
    }
    async loadGames() {
        const data = await this.model.all()
        this.setState({
            loader: false,
            data: data
        })
    }

    render() {
        return (
            <Fragment>
                <Navbar />
                <Container >
                    <h2>Juegos creados</h2>
                    {this.state.loader ? <Loader /> : ""}
                    <Grid container spacing={2}>
                        {
                            this.state.data.map(({ pk, fields: data }) => (
                                <Grid key={pk} item xs={12} sm={6} md={3}>
                                    <Card elevation={2}>
                                        <CardContent>
                                            Pin: <h3 style={{ display: "inline" }}>{data.pin.toUpperCase()}</h3><br />
                                            {data.user.username}<br />
                                            {data.user.is_staff ? (<b>Teacher</b>) : (<b>Student</b>)}
                                        </CardContent>
                                        <CardActions style={{ float: "right" }}>
                                            <Button onClick={(e) => this.props.history.push(`/games/detail/${data.pin}`)} color="primary" >
                                                Detalle...
                                            </Button>
                                        </CardActions>
                                    </Card>
                                </Grid>
                            ))
                        }
                    </Grid>
                </Container>
                {this.state.data.length ? ("") : (<EmptyMessage title="Woops!" message="No hay usuarios registrados" />)}
            </Fragment>
        )
    }
}