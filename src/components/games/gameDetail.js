import React, { Fragment, Component } from "react"
import {
    Card,
    CardContent,
    Container,
    Grid,
} from '@material-ui/core';
import Navbar from './../default/navbar'
import Game from '../../models/Game';
import EmptyMessage from '../default/emptyMessage';
import {
    BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend,
} from 'recharts';
import { format } from "./../../config"
export default class GameDetail extends Component {
    constructor(props) {
        super(props)
        this.model = new Game();
        this.state = {
            pin: props.match.params.pin,
            data: []
        }
    }

    componentDidMount() {
        this.loadData(this.state.pin)
    }

    async loadData(pin) {
        const data = await this.model.detail(pin)
        console.log(data)
        let newData = data.map(elem => {
            const total_sell = parseInt(elem.data_statics.total_bottle_sells)
            return {
                name: elem.name,
                Grupos: total_sell,
            }
        })
        this.setState({
            data: newData
        })
    }


    render() {
        let thereisdata = this.state.data.length ? true : false
        return (
            <Fragment>
                <Navbar />
                <Container >
                    <Grid style={{ "marginTop": "40px" }} container spacing={2}>
                        {thereisdata ? ("") : (<EmptyMessage title="Woops!" message="Aún no hay participacion de ningun grupo" />)}
                        <Grid item xs={12} sm={12} md={2} />
                        <Grid item xs={12} sm={6} md={8}>
                            <Card elevation={3} style={{ display: `${thereisdata ? "block" : "none"}` }}>
                                <CardContent style={{ textAlign: "center" }}>
                                    <h3>Detalle del juego por Grupos</h3>
                                    <BarChart
                                        width={700}
                                        height={300}
                                        data={this.state.data}
                                        margin={{
                                            top: 5, right: 30, left: 50, bottom: 5,
                                        }}
                                        barSize={20}
                                    >
                                        <XAxis dataKey="name" scale="point" padding={{ left: 10, right: 10 }} />
                                        <YAxis />
                                        <Tooltip />
                                        <Legend />
                                        <CartesianGrid strokeDasharray="3 3" />
                                        <Bar dataKey="Grupos" fill="#8884d8" background={{ fill: '#efefef' }} />
                                    </BarChart>
                                </CardContent>

                            </Card>
                        </Grid>
                        <Grid item xs={12} sm={12} md={8} />
                    </Grid>
                </Container>
            </Fragment>
        );

    }
}