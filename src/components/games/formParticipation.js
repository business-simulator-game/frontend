import React, { Component, Fragment, createRef } from 'react';
import {
    Card,
    CardContent,
    Container,
    Button,
    Grid,
    TextField,
    FormControl
} from '@material-ui/core';
import IconCreate from "@material-ui/icons/Add"
import Loader from "./../default/loader"
import { AlertError, AlertSuccess, AlertWarning } from "./../default/alert"
import Participation from '../../models/Participation';
import { sound, format } from "./../../config"
import { Redirect } from "react-router-dom"

export default class GameCreate extends Component {
    constructor(props) {
        super(props);
        let { round_id, user_id, time, budget, rounds, game_id, pin } = props.data;
        console.log(pin)
        this.model = new Participation();
        this.state = {
            success: false,
            error: false,
            warning: false,
            loader: false,
            number: time,
            rounds: rounds,
            round_id: round_id,
            user_id: user_id,
            budget_init: budget,
            budget: budget,
            color: "green",
            pin: pin,
            game_id: game_id,
            idinterval: 0

        }
        this.form = createRef()
        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleChange = this.handleChange.bind(this)
        sound.init()
    }
    componentDidMount() {
        sound.play()
        this.setIntervalGo()

    }
    setIntervalGo() {
        const time = this.state.number
        let idInterval = setInterval(() => {
            let color = this.state.color;
            if (this.state.number < time / 2) {
                color = "#e67e22"
            } else if (this.state.number < time / 3) {
                color = "#e74c3c"
            }
            if (this.state.number == 1) {
                clearInterval(idInterval);
                this.sendDataRound()
                sound.pause()
            }
            this.setState({ number: this.state.number - 1, color: color })
        }, 1000);
        this.setState({ ...this.state, idinterval: idInterval })
    }
    componentWillUnmount() {
        sound.pause()
    }
    async handleSubmit(e) {
        e.preventDefault();
        clearInterval(this.state.idinterval)
        this.sendDataRound()
    }

    async sendDataRound() {
        this.setState({ loader: true });

        const form = document.querySelector("#form");
        let data = new FormData(form);
        try {
            let res = await this.model.create(data);
            let newState = this.state;
            if (res.type === "max_round_allowed") {
                alert("Felicidades ahora miremos como les fue a todos!");
                window.location.href = window.location.origin + `/games/detail/${this.state.pin}`
                // this.props.history.push(`/games/detail/${this.state.pin}`)
                return
            }
            if (res) {
                newState.rounds = res.data.rounds
                newState.number = res.data.time
                newState.round_id = res.data.round_id
                newState.error = false;
                newState.success = true;
                form.reset()
                this.setIntervalGo()
            } else {
                sound.pause();

                newState.error = true;
                newState.success = false;
            }
            newState.loader = false;
            this.setState({ ...newState });
        } catch (err) {
            console.error(err)
        }
    }

    handleChange(e) {
        // don't touch
        const total = format.clear(e.target.value);
        e.target.value = format.convert(total)
        // end
        const inputsTwo = document.querySelector("#second-forms").children
        const total_spend = this.helpNumbers(inputsTwo).reduce((sum, number) => sum + number)
        const result = this.state.budget_init - total_spend;
        if (result < 0) {
            this.setState({ warning: true })
        } else {
            this.setState({ warning: false })
        }
        this.setState({ budget: result })
    }
    helpNumbers(objects) {
        return Array.from(objects).map(elem => {
            let number = elem.firstElementChild.firstElementChild.nextElementSibling.firstElementChild.value
            return parseInt(format.clear(number)) || 0
        })
    }
    render() {
        let { budget, user_id, round_id, game_id } = this.state
        return (
            <Fragment>
                <Container style={{ marginTop: 30, marginBottom: 50 }} >
                    <Grid container spacing={2}>
                        <Grid item xs={12} sm={12} md={2} >
                            <h1 style={{ fontSize: 40, display: "inline" }}>Ronda {this.state.rounds} </h1>
                            <div style={{ marginTop: 90 }}>
                                <p>Tiempo restante:</p>
                                <h1 style={{ fontSize: 50, color: this.state.color, display: "inline" }}>{this.state.number} </h1> Segundos
                            </div>
                        </Grid>
                        <Grid item xs={12} sm={6} md={8}>
                            <Card elevation={5}>
                                {this.state.error ? (<AlertError message="Error, Húbo un problema!" />) : ('')}
                                {this.state.success ? (<AlertSuccess message="Hemos registrado los resultados de esta ronda!" />) : ('')}
                                {this.state.warning ? (<AlertWarning message="Heyy!! Te pasaste del presupuesto (esto afectará el resultado final)" />) : ('')}
                                {this.state.loader ? (<Loader />) : ("")}
                                <h2 style={{ textAlign: "center" }}>Jugando....</h2>
                                <h3 style={{ marginLeft: 20 }}>Presupuesto: <em>$ {format.convert(budget)}</em></h3>
                                <CardContent style={{ textAlign: "center" }}>
                                    <form onSubmit={this.handleSubmit} id="form" ref={this.form}>
                                        <input type="hidden" name="user_id" value={user_id} />
                                        <input type="hidden" name="round_id" value={round_id} />
                                        <input type="hidden" name="game_id" value={game_id} />
                                        <input type="hidden" name="budget" value={budget} />
                                        <h4 style={{ marginTop: -20 }}>Botellas y barriles...</h4>
                                        <Grid id="first-forms" container spacing={1}>
                                            <Grid item xs={12} md={6}>
                                                <TextField
                                                    fullWidth
                                                    name="price_barrel_sell"
                                                    id="outlined-number"
                                                    label="Precio de venta por barril"
                                                    type="text"
                                                    onChange={this.handleChange}
                                                    InputLabelProps={{
                                                        shrink: true,
                                                    }}
                                                    placeholder="Precio por barril"
                                                    variant="outlined"
                                                    required
                                                />
                                            </Grid>

                                            <Grid item xs={12} md={6}>
                                                <TextField
                                                    fullWidth
                                                    onChange={this.handleChange}
                                                    name="price_bottle_sell"
                                                    id="outlined-number"
                                                    label="Precio de venta por botella"
                                                    type="text"
                                                    InputLabelProps={{
                                                        shrink: true,
                                                    }}
                                                    placeholder="Precio por botella"
                                                    variant="outlined"
                                                    required
                                                />
                                            </Grid>
                                            <Grid item xs={12} md={6}>
                                                <TextField
                                                    fullWidth
                                                    onChange={this.handleChange}
                                                    name="quantity_barrel_sell"
                                                    id="outlined-number"
                                                    label="Cantidad de barriles por vender"
                                                    type="text"
                                                    InputLabelProps={{
                                                        shrink: true,
                                                    }}
                                                    placeholder="Cantidad de barriles.."
                                                    variant="outlined"
                                                    required
                                                />
                                            </Grid>
                                            <Grid item xs={12} md={6}>
                                                <TextField
                                                    fullWidth
                                                    onChange={this.handleChange}
                                                    id="outlined-number"
                                                    name="quantity_bottle_sell"
                                                    label="Cantidad de botellas por vender"
                                                    type="text"
                                                    InputLabelProps={{
                                                        shrink: true,
                                                    }}
                                                    placeholder="Cantidad de botellas"
                                                    variant="outlined"
                                                    required
                                                />
                                            </Grid>
                                        </Grid>
                                        <h4>Reparticion presupuesto</h4>
                                        <Grid container id="second-forms" spacing={1}>
                                            <Grid item xs={12} md={6}>
                                                <TextField
                                                    fullWidth
                                                    onChange={this.handleChange}
                                                    name="spend_marketing"
                                                    id="outlined-number"
                                                    label="Marketing"
                                                    type="text"
                                                    InputLabelProps={{
                                                        shrink: true,
                                                    }}
                                                    placeholder="Dinero para el marketing"
                                                    variant="outlined"
                                                    required
                                                />
                                            </Grid>
                                            <Grid item xs={12} md={6}>
                                                <TextField
                                                    fullWidth
                                                    onChange={this.handleChange}
                                                    name="spend_machine"
                                                    id="outlined-number"
                                                    label="Maquinaria"
                                                    type="text"
                                                    InputLabelProps={{
                                                        shrink: true,
                                                    }}
                                                    placeholder="Dinero para la maquinaria y producción"
                                                    variant="outlined"
                                                    required
                                                />
                                            </Grid>
                                            <Grid item xs={12} md={6}>
                                                <TextField
                                                    fullWidth
                                                    onChange={this.handleChange}
                                                    id="outlined-number"
                                                    name="spend_personal"
                                                    label="Personal"
                                                    type="text"
                                                    InputLabelProps={{
                                                        shrink: true,
                                                    }}
                                                    placeholder="Dinero para el personal de la empresa"
                                                    variant="outlined"
                                                    required
                                                />
                                            </Grid>
                                            <Grid item xs={12} md={6}>
                                                <TextField
                                                    fullWidth
                                                    onChange={this.handleChange}
                                                    id="outlined-number"
                                                    name="spend_supplies"
                                                    label="Materia prima"
                                                    type="text"
                                                    InputLabelProps={{
                                                        shrink: true,
                                                    }}
                                                    placeholder="Dinero para la materia prima del producto"
                                                    variant="outlined"
                                                    required
                                                />
                                            </Grid>
                                        </Grid>
                                        <FormControl fullWidth>
                                            <Button
                                                style={{ marginTop: 30 }}
                                                variant="contained"
                                                color="primary"
                                                type="submit"
                                            >
                                                <IconCreate />    Terminar ronda
                                    </Button>
                                        </FormControl>
                                    </form>

                                </CardContent>

                            </Card>
                        </Grid>
                        <Grid item xs={12} sm={12} md={2} />
                    </Grid>
                </Container>
            </Fragment>
        )
    }
}