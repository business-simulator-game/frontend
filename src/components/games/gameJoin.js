
import React, { Component, Fragment } from 'react';
import { localdb } from "./../../config"
import {
    Card,
    CardContent,
    Container,
    Button,
    Grid,
    TextField,
} from '@material-ui/core';
import FormParticipation from "./formParticipation"
import Loader from "./../default/loader"
import Game from '../../models/Game';
import Navbar from './../default/navbar'
import { AlertError, AlertSuccess } from '../default/alert';
export default class GameJoin extends Component {
    constructor(props) {
        super(props);
        this.game = new Game();
        this.state = {
            loader: false,
            error: false,
            success: false,
            initRound: false,
        }
        this.handleClick = this.handleClick.bind(this)
    }
    async handleClick(e) {
        e.preventDefault()
        this.setState({ loader: true })
        const data = new FormData()
        console.log(e.target.pin.value)
        data.append("pin", e.target.pin.value)
        data.append("id_user", localdb.getId())
        const res = await this.game.join(data)
        let newState = this.state;
        if (res) {
            newState.error = false;
            newState.success = true;
            newState.initRound = res
        } else {
            newState.error = true;
            newState.success = false;
        }
        newState.loader = false;
        this.setState({ ...newState });
    }

    render() {
        return (
            <Fragment>
                <Navbar />
                {
                    !this.state.initRound ? (


                        <Container >
                            <h2 style={{ textAlign: "center" }}>Ingresar pin  de juego</h2>
                            <Grid container spacing={2}>
                                <Grid item xs={12} sm={12} md={3} />
                                <Grid item xs={12} sm={6} md={6}>
                                    {this.state.error ? (<AlertError message="El pin no es valido!" />) : ('')}
                                    {this.state.success ? (<AlertSuccess message="Que comience el juego!" />) : ('')}
                                    <Card elevation={5}>
                                        {this.state.loader ? (<Loader />) : ("")}
                                        <CardContent style={{ textAlign: "center" }}>
                                            <form onSubmit={this.handleClick}>
                                                <TextField
                                                    name="pin"
                                                    variant="outlined"
                                                    label="Ingresa el PIN"
                                                />
                                                <Button type="submit" style={{ marginTop: "10px ", marginLeft: "10px" }} variant="contained" color="secondary">
                                                    Unirme
                                    </Button>
                                            </form>
                                        </CardContent>

                                    </Card>
                                </Grid>
                                <Grid item xs={12} sm={12} md={3} />
                            </Grid>
                        </Container>
                    ) : ("")
                }
                {
                    this.state.initRound ? (
                        <FormParticipation data={this.state.initRound} />
                    ) : ("")
                }
            </Fragment >
        )
    }
}