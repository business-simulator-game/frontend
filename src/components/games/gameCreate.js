import React, { Component, Fragment } from 'react';
import {
    Card,
    CardContent,
    Container,
    Button,
    Grid,
} from '@material-ui/core';
import { Link } from "react-router-dom"
import IconCreate from "@material-ui/icons/Create"
import Loader from "./../default/loader"
import { AlertError, AlertSuccess } from "./../default/alert"
import Game from '../../models/Game';
import Navbar from './../default/navbar'
import FormParticipation from "./../games/formParticipation"
export default class GameCreate extends Component {
    constructor(props) {
        super(props);
        this.model = new Game();
        this.state = {
            pin: false,
            success: false,
            error: false
        }
        this.handleClick = this.handleClick.bind(this)
    }
    async handleClick(e) {
        const pin = Math.floor(Math.random() * 1000000);
        const user = JSON.parse(localStorage.getItem("session-user"))
        this.setState({ loader: true });
        let data = new FormData();
        data.append("id_user_admin", user.id)
        data.append("pin", pin)
        let res = await this.model.create(data);
        let newState = this.state;
        if (res) {
            newState.error = false;
            newState.success = true;
            newState.pin = pin
        } else {
            newState.error = true;
            newState.success = false;
        }
        newState.loader = false;
        this.setState({ ...newState });
    }
    render() {
        return (
            <Fragment>
                <Navbar />
                <Container >
                    <h2 style={{ textAlign: "center" }}>Crear pin  de juego</h2>
                    <Grid container spacing={2}>
                        <Grid item xs={12} sm={12} md={3} />
                        <Grid item xs={12} sm={6} md={6}>
                            {this.state.error ? (<AlertError message="Error, Húbo un problema!" />) : ('')}
                            {this.state.success ? (<AlertSuccess message="Has creado un pin de juego!" />) : ('')}
                            {this.state.loader ? (<Loader />) : ("")}
                            <Card elevation={5}>
                                <CardContent style={{ textAlign: "center" }}>
                                    <Button onClick={this.handleClick} variant="contained" color={this.state.pin ? ("primary") : ("secondary")}  >
                                        <IconCreate /> &nbsp;
                                        {this.state.pin ? ("Crear otro PIN") : (" Generar PIN de juego")}
                                    </Button>
                                    {this.state.pin ? (
                                        <Fragment>
                                            <p>Ahora puedes compartir este pin..</p>
                                            <h2>{this.state.pin}</h2>
                                            <Link to="/games/list">Ver juegos</Link>
                                        </Fragment>
                                    ) : ("")}
                                </CardContent>

                            </Card>
                        </Grid>
                        <Grid item xs={12} sm={12} md={3} />
                    </Grid>
                </Container>

            </Fragment>
        )
    }
}