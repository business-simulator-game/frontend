
import React, { Component, Fragment } from 'react';
import {
    Card,
    CardActions,
    CardContent,
    Container,
    Button,
    CircularProgress,
    Grid,
} from '@material-ui/core';
import IconDelete from "@material-ui/icons/Delete"
import User from '../../models/User';
import Navbar from './../default/navbar'
import EmptyMessage from '../default/emptyMessage';
export default class UserList extends Component {
    constructor(props) {
        super(props);
        this.user = new User();
        this.state = {
            loader: true,
            data: []
        }
        this.getUsers = this.getUsers.bind(this)
    }
    componentDidMount() {
        this.getUsers();
    }
    async getUsers() {
        const users = await this.user.all()
        this.setState({
            loader: false,
            data: users
        })
    }

    render() {
        console.log(this.state.data)
        return (
            <Fragment>
                <Navbar />
                <Container >
                    <h2>Listado de usuarios</h2>
                    {this.state.loader ?
                        <div style={{ display: "flex" }}>
                            <CircularProgress style={{ margin: "0 auto" }} />
                        </div>
                        : ""
                    }
                    <Grid container spacing={2}>
                        {
                            this.state.data.map(({ pk, fields: user }) => (
                                <Grid key={pk} item xs={12} sm={6} md={4}>
                                    <Card elevation={2}>
                                        <CardContent>
                                            {user.email}<br />
                                            {user.username}<br />
                                            {user.is_staff ? (<b>Teacher</b>) : (<b>Student</b>)}
                                        </CardContent>
                                        <CardActions style={{ float: "right" }}>
                                            {/* <Button onClick={(e) => alert("Oh espera")} variant="outlined" color="secondary" >
                                                <IconDelete />
                                            </Button> */}
                                        </CardActions>
                                    </Card>
                                </Grid>
                            ))
                        }
                    </Grid>
                </Container>
                {this.state.data.length ? ("") : (<EmptyMessage title="Woops!" message="No hay usuarios registrados" />)}
            </Fragment>
        )
    }
}