import Navbar from './../default/navbar';
import React, { Fragment } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    margin: '0 auto',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    overflow: 'hidden',
    marginTop: 50,
    backgroundColor: 'theme.palette.background.paper',
  },
  gridList: {
    width: 500,
    height: 600,
  },
}));

const data = [
  {
    img:
      'https://blog.naturlider.com/wp-content/uploads/2020/03/AdobeStock_309195144-post-dia-mundial-naturaleza.jpeg',
    cols: 3,
    title: 'Paisaje de atardecer...',
  },
  {
    img:
      'https://concepto.de/wp-content/uploads/2015/03/naturaleza-medio-ambiente-e1505407093531.jpeg',
    cols: 3,
    title: 'Dia soleado al lado de un árbol',
  },
];

const Home = () => {
  const classes = useStyles();

  return (
    <Fragment>
      <Navbar />
      <div className={classes.root}>
        <GridList cellHeight={160} className={classes.gridList} cols={3}>
          {data.map((tile) => (
            <GridListTile
              style={{ height: 250 }}
              key={tile.img}
              cols={tile.cols || 1}
            >
              <h5>{tile.title}</h5>
              <img style={{ height: '100%' }} src={tile.img} alt={tile.title} />
            </GridListTile>
          ))}
        </GridList>
      </div>
    </Fragment>
  );
};
export default Home;
