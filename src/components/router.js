import React from "react"
import { BrowserRouter, Redirect, Route, Switch } from "react-router-dom"
import NotFound from "./default/notFound"
import Login from "./auth/login"
import Register from "./auth/register"
import Home from "./main/home"
import PrivateRoute from "./privateRoute"
import UserList from "./users/userList"
import GameCreate from "./games/gameCreate"
import GameJoin from "./games/gameJoin"
import GameList from "./games/gameList"
import GameDetail from "./games/gameDetail"
import GroupCreate from "./groups/groupCreate"
import GroupList from "./groups/groupList"


const Router = () => {
    return (
        <BrowserRouter>
            <Switch>
                <Route exact path="/" render={something => (<Redirect to="/login" />)} />
                <Route exact path="/login" component={Login} />
                <Route exact path="/register" component={Register} />
                <PrivateRoute exact path="/home" component={Home} />
                <PrivateRoute exact path="/users" component={UserList} />
                <PrivateRoute exact path="/games/create" component={GameCreate} />
                <PrivateRoute exact path="/games/join" component={GameJoin} />
                <PrivateRoute exact path="/games/list" component={GameList} />
                <PrivateRoute exact path="/groups/list" component={GroupList} />
                <PrivateRoute exact path="/groups/create" component={GroupCreate} />
                <PrivateRoute exact path="/games/detail/:pin" component={GameDetail} />

                <Route component={NotFound} />
            </Switch>
        </BrowserRouter>
    );
}

export default Router;